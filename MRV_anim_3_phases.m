
clear all
close all
clc

addpath("~/.wine/drive_c/femm42/mfiles");
savepath;

%Paramètres dimensionnels (Attention donné en mm, comme dans FEMM)

%profondeur (mm)
prof=150;	


%Stator
R_stat_ext=22;
R_stat_enc=18;
R_stat_int=12;
Angle_arc_dents_stator=pi/6;


%entrefer
entrefer=0.5;

%Rotor
Rr_ext=11.5;
Rr_int=Rr_ext/2;
Angle_arc_dents_rotor=pi/4;

%Arbre moteur
R_arbre=1.5;


%Carac bobines
Ns=200;
I=5;


%Construction encoches

position=[0:1:90];
I1=zeros(1,length(position));
I2=zeros(1,length(position));
I3=zeros(1,length(position));

I1(position>=45)=I;
I2(position>=15 & position<=60)=I;
I3(position<=30 | position>=75)=I;


%position=0*pi/180;

for m=1:length(position)
 
 pos=position(m)*pi/180;


%closefemm
openfemm;

newdocument(0); % 2 correspond a un problème heatflow

main_maximize;
mi_probdef(0,'millimeters','planar',1e-8,prof,30);

%propriétés des matériaux
%exemple mi_addmaterial(’matname’, mu x, mu y, H c, J, Cduct, Lam d, Phi hmax, lam fill,LamType, Phi hx, Phi hy, nstr, dwire)
mu_r=5000;
mi_addmaterial('Air', 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0);       % 
mi_addmaterial('Cuivre', 1, 1, 0, 0 ,0, 0, 0, 1, 0, 0, 0);   % 
mi_addmaterial('Fer_lin', mu_r, mu_r, 0, 0, 0, 0, 0, 1, 0, 0, 0); % 

%%mi_addcircprop('circuit name', Val_i , circuit_type)
%Ajoute un circuit avec son nom, la valeur du courant et le type de connection (0 : parallel ; 1 : serie)
mi_addcircprop('Bob1+', I1(m) , 1)
mi_addcircprop('Bob1-', -I1(m) , 1)
mi_addcircprop('Bob2+', I2(m) , 1)
mi_addcircprop('Bob2-', -I2(m) , 1)
mi_addcircprop('Bob3+', I3(m) , 1)
mi_addcircprop('Bob3-', -I3(m) , 1)

%Construction stator
%Circuit magnetique
mi_drawarc(R_stat_ext,0,-R_stat_ext,0,180,1)
mi_drawarc(-R_stat_ext,0,R_stat_ext,0,180,1)
mi_drawarc(R_stat_int,0,-R_stat_int,0,180,1)
mi_drawarc(-R_stat_int,0,R_stat_int,0,180,1)
mi_drawline(R_stat_enc*cos(Angle_arc_dents_stator/2),R_stat_enc*sin(Angle_arc_dents_stator/2),...
				R_stat_int*cos(Angle_arc_dents_stator/2),R_stat_int*sin(Angle_arc_dents_stator/2))
mi_drawarc(R_stat_enc*cos(Angle_arc_dents_stator/2),R_stat_enc*sin(Angle_arc_dents_stator/2),...
				R_stat_enc*cos(3*Angle_arc_dents_stator/2),R_stat_enc*sin(3*Angle_arc_dents_stator/2),...
				Angle_arc_dents_stator*180/pi,1)
mi_drawline(R_stat_enc*cos(3*Angle_arc_dents_stator/2),R_stat_enc*sin(3*Angle_arc_dents_stator/2),...
				R_stat_int*cos(3*Angle_arc_dents_stator/2),R_stat_int*sin(3*Angle_arc_dents_stator/2))				
mi_drawline(R_stat_enc*cos(5*Angle_arc_dents_stator/2),R_stat_enc*sin(5*Angle_arc_dents_stator/2),...
				R_stat_int*cos(5*Angle_arc_dents_stator/2),R_stat_int*sin(5*Angle_arc_dents_stator/2))
mi_drawarc(R_stat_enc*cos(5*Angle_arc_dents_stator/2),R_stat_enc*sin(5*Angle_arc_dents_stator/2),...
				R_stat_enc*cos(7*Angle_arc_dents_stator/2),R_stat_enc*sin(7*Angle_arc_dents_stator/2),...
				Angle_arc_dents_stator*180/pi,1)				
mi_drawline(R_stat_enc*cos(7*Angle_arc_dents_stator/2),R_stat_enc*sin(7*Angle_arc_dents_stator/2),...
				R_stat_int*cos(7*Angle_arc_dents_stator/2),R_stat_int*sin(7*Angle_arc_dents_stator/2))
mi_drawline(R_stat_enc*cos(9*Angle_arc_dents_stator/2),R_stat_enc*sin(9*Angle_arc_dents_stator/2),...
				R_stat_int*cos(9*Angle_arc_dents_stator/2),R_stat_int*sin(9*Angle_arc_dents_stator/2))
mi_drawarc(R_stat_enc*cos(9*Angle_arc_dents_stator/2),R_stat_enc*sin(9*Angle_arc_dents_stator/2),...
				R_stat_enc*cos(11*Angle_arc_dents_stator/2),R_stat_enc*sin(11*Angle_arc_dents_stator/2),...
				Angle_arc_dents_stator*180/pi,1)	
mi_drawline(R_stat_enc*cos(11*Angle_arc_dents_stator/2),R_stat_enc*sin(11*Angle_arc_dents_stator/2),...
				R_stat_int*cos(11*Angle_arc_dents_stator/2),R_stat_int*sin(11*Angle_arc_dents_stator/2))				
mi_drawline(R_stat_enc*cos(13*Angle_arc_dents_stator/2),R_stat_enc*sin(13*Angle_arc_dents_stator/2),...
				R_stat_int*cos(13*Angle_arc_dents_stator/2),R_stat_int*sin(13*Angle_arc_dents_stator/2))
mi_drawarc(R_stat_enc*cos(13*Angle_arc_dents_stator/2),R_stat_enc*sin(13*Angle_arc_dents_stator/2),...
				R_stat_enc*cos(15*Angle_arc_dents_stator/2),R_stat_enc*sin(15*Angle_arc_dents_stator/2),...
				Angle_arc_dents_stator*180/pi,1)	
mi_drawline(R_stat_enc*cos(15*Angle_arc_dents_stator/2),R_stat_enc*sin(15*Angle_arc_dents_stator/2),...
				R_stat_int*cos(15*Angle_arc_dents_stator/2),R_stat_int*sin(15*Angle_arc_dents_stator/2))				
mi_drawline(R_stat_enc*cos(17*Angle_arc_dents_stator/2),R_stat_enc*sin(17*Angle_arc_dents_stator/2),...
				R_stat_int*cos(17*Angle_arc_dents_stator/2),R_stat_int*sin(17*Angle_arc_dents_stator/2))
mi_drawarc(R_stat_enc*cos(17*Angle_arc_dents_stator/2),R_stat_enc*sin(17*Angle_arc_dents_stator/2),...
				R_stat_enc*cos(19*Angle_arc_dents_stator/2),R_stat_enc*sin(19*Angle_arc_dents_stator/2),...
				Angle_arc_dents_stator*180/pi,1)
mi_drawline(R_stat_enc*cos(19*Angle_arc_dents_stator/2),R_stat_enc*sin(19*Angle_arc_dents_stator/2),...
				R_stat_int*cos(19*Angle_arc_dents_stator/2),R_stat_int*sin(19*Angle_arc_dents_stator/2))												
mi_drawline(R_stat_enc*cos(21*Angle_arc_dents_stator/2),R_stat_enc*sin(21*Angle_arc_dents_stator/2),...
				R_stat_int*cos(21*Angle_arc_dents_stator/2),R_stat_int*sin(21*Angle_arc_dents_stator/2))								
mi_drawarc(R_stat_enc*cos(21*Angle_arc_dents_stator/2),R_stat_enc*sin(21*Angle_arc_dents_stator/2),...
				R_stat_enc*cos(23*Angle_arc_dents_stator/2),R_stat_enc*sin(23*Angle_arc_dents_stator/2),...
				Angle_arc_dents_stator*180/pi,1)	
mi_drawline(R_stat_enc*cos(23*Angle_arc_dents_stator/2),R_stat_enc*sin(23*Angle_arc_dents_stator/2),...
				R_stat_int*cos(23*Angle_arc_dents_stator/2),R_stat_int*sin(23*Angle_arc_dents_stator/2))				
mi_addblocklabel((R_stat_ext+R_stat_enc)/2,0)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel((R_stat_ext+R_stat_enc)/2,0);  %Sélectionne le label le plus proche
%mi setblockprop(’blockname’, automesh, meshsize, ’incircuit’, magdir, group,turns) affecte les matériaux à la zone sélectionnée
mi_setblockprop('Fer_lin',0,0,'<None>',0,0,0)	%Affecte la propriété aux label délectionné
mi_clearselected
%Bobines
angles_bob=[2*Angle_arc_dents_stator/2: 4*Angle_arc_dents_stator/2:24*Angle_arc_dents_stator/2] ;

for k=1:length(angles_bob)
 	mi_drawline(R_stat_enc*cos(angles_bob(k)),R_stat_enc*sin(angles_bob(k)),...
					R_stat_int*cos(angles_bob(k)),R_stat_int*sin(angles_bob(k)) )
endfor

%Affectation_bobines
dec_bob=pi/24;
%bob1
X_bob1_m=[(R_stat_enc+R_stat_int)/2*cos(pi/12+dec_bob)];
Y_bob1_m=[(R_stat_enc+R_stat_int)/2*sin(pi/12+dec_bob)];
mi_addblocklabel(X_bob1_m,Y_bob1_m)	%Ajoute un point de propriétés aux coordonnées
mi_addblocklabel(-X_bob1_m,Y_bob1_m)	%Ajoute un point de propriétés aux coordonnées)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel(X_bob1_m,Y_bob1_m)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel(-X_bob1_m,Y_bob1_m)	%Ajoute un point de propriétés aux coordonnées
mi_setblockprop('Cuivre',0,0,'Bob1-',0,0,Ns)	%Affecte la propriété aux label délectionné
mi_clearselected
mi_addblocklabel(X_bob1_m,-Y_bob1_m)	%Ajoute un point de propriétés aux coordonnées
mi_addblocklabel(-X_bob1_m,-Y_bob1_m)	%Ajoute un point de propriétés aux coordonnées)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel(X_bob1_m,-Y_bob1_m)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel(-X_bob1_m,-Y_bob1_m)	%Ajoute un point de propriétés aux coordonnées
mi_setblockprop('Cuivre',0,0,'Bob1+',0,0,Ns)	%Affecte la propriété aux label délection
mi_clearselected
%bob2
X_bob2_m=[(R_stat_enc+R_stat_int)/2*cos(2*pi/12+dec_bob)];
Y_bob2_m=[(R_stat_enc+R_stat_int)/2*sin(2*pi/12+dec_bob)];
mi_addblocklabel(X_bob2_m,Y_bob2_m)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel(X_bob2_m,Y_bob2_m)	%Ajoute un point de propriétés aux coordonnées
mi_setblockprop('Cuivre',0,0,'Bob2-',0,0,Ns)	%Affecte la propriété aux label délectionné
mi_clearselected
mi_addblocklabel(-X_bob2_m,-Y_bob2_m)	%Ajoute un point de propriétés aux coordonnées)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel(-X_bob2_m,-Y_bob2_m)	%Ajoute un point de propriétés aux coordonnées
mi_setblockprop('Cuivre',0,0,'Bob2+',0,0,Ns)	%Affecte la propriété aux label délectionné
mi_clearselected
X_bob2_m=[(R_stat_enc+R_stat_int)/2*cos(5*pi/12+dec_bob)];
Y_bob2_m=[(R_stat_enc+R_stat_int)/2*sin(5*pi/12+dec_bob)];
mi_addblocklabel(X_bob2_m,Y_bob2_m)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel(X_bob2_m,Y_bob2_m)	%Ajoute un point de propriétés aux coordonnées
mi_setblockprop('Cuivre',0,0,'Bob2+',0,0,Ns)	%Affecte la propriété aux label délection
mi_clearselected
mi_addblocklabel(-X_bob2_m,-Y_bob2_m)	%Ajoute un point de propriétés aux coordonnées)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel(-X_bob2_m,-Y_bob2_m)	%Ajoute un point de propriétés aux coordonnées
mi_setblockprop('Cuivre',0,0,'Bob2-',0,0,Ns)	%Affecte la propriété aux label délection
mi_clearselected
%bob2
X_bob3=[(R_stat_enc+R_stat_int)/2*cos(pi/2+dec_bob)];
Y_bob3=[(R_stat_enc+R_stat_int)/2*sin(pi/2+dec_bob)];
mi_addblocklabel(X_bob3,Y_bob3)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel(X_bob3,Y_bob3)	%Ajoute un point de propriétés aux coordonnées
mi_setblockprop('Cuivre',0,0,'Bob3+',0,0,Ns)	%Affecte la propriété aux label délectionné
mi_clearselected
mi_addblocklabel(-X_bob3,-Y_bob3)	%Ajoute un point de propriétés aux coordonnées)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel(-X_bob3,-Y_bob3)	%Ajoute un point de propriétés aux coordonnées
mi_setblockprop('Cuivre',0,0,'Bob3-',0,0,Ns)	%Affecte la propriété aux label délectionné
mi_clearselected
X_bob3=[(R_stat_enc+R_stat_int)/2*cos(9*pi/12+dec_bob)];
Y_bob3=[(R_stat_enc+R_stat_int)/2*sin(9*pi/12+dec_bob)];
mi_addblocklabel(X_bob3,Y_bob3)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel(X_bob3,Y_bob3)	%Ajoute un point de propriétés aux coordonnées
mi_setblockprop('Cuivre',0,0,'Bob3-',0,0,Ns)	%Affecte la propriété aux label délection
mi_clearselected
mi_addblocklabel(-X_bob3,-Y_bob3)	%Ajoute un point de propriétés aux coordonnées)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel(-X_bob3,-Y_bob3)	%Ajoute un point de propriétés aux coordonnées
mi_setblockprop('Cuivre',0,0,'Bob3+',0,0,Ns)	%Affecte la propriété aux label délection
mi_clearselected

%Construction entrefer

mi_drawarc(Rr_ext,0,-Rr_ext,0,180,1)
mi_drawarc(-Rr_ext,0,Rr_ext,0,180,1)
mi_addblocklabel(Rr_ext+entrefer/2,0)	%Ajoute un point de propriétés aux coordonnées)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel(Rr_ext+entrefer/2,0)
mi_setblockprop('Air',0,0,'<None>',0,0,0)	%Affecte la propriété aux label délectionné
mi_clearselected
%Construction rotor
%Angle_arc_dents_rotor=pi/4;
mi_drawline(Rr_ext*cos(Angle_arc_dents_rotor/2+pos),Rr_ext*sin(Angle_arc_dents_rotor/2+pos),...
				Rr_int*cos(Angle_arc_dents_rotor/2+pos),Rr_int*sin(Angle_arc_dents_rotor/2+pos))
mi_drawline(Rr_ext*cos(-Angle_arc_dents_rotor/2+pos),Rr_ext*sin(-Angle_arc_dents_rotor/2+pos),...
				Rr_int*cos(-Angle_arc_dents_rotor/2+pos),Rr_int*sin(-Angle_arc_dents_rotor/2+pos))
mi_drawline(Rr_ext*cos(Angle_arc_dents_rotor/2+pi/2+pos),Rr_ext*sin(Angle_arc_dents_rotor/2+pi/2+pos),...
				Rr_int*cos(Angle_arc_dents_rotor/2+pi/2+pos),Rr_int*sin(Angle_arc_dents_rotor/2+pi/2+pos))
mi_drawline(Rr_ext*cos(-Angle_arc_dents_rotor/2+pi/2+pos),Rr_ext*sin(-Angle_arc_dents_rotor/2+pi/2+pos),...
				Rr_int*cos(-Angle_arc_dents_rotor/2+pi/2+pos),Rr_int*sin(-Angle_arc_dents_rotor/2+pi/2+pos))
mi_drawline(Rr_ext*cos(Angle_arc_dents_rotor/2+2*pi/2+pos),Rr_ext*sin(Angle_arc_dents_rotor/2+2*pi/2+pos),...
				Rr_int*cos(Angle_arc_dents_rotor/2+2*pi/2+pos),Rr_int*sin(Angle_arc_dents_rotor/2+2*pi/2+pos))
mi_drawline(Rr_ext*cos(-Angle_arc_dents_rotor/2+2*pi/2+pos),Rr_ext*sin(-Angle_arc_dents_rotor/2+2*pi/2+pos),...
				Rr_int*cos(-Angle_arc_dents_rotor/2+2*pi/2+pos),Rr_int*sin(-Angle_arc_dents_rotor/2+2*pi/2+pos))
mi_drawline(Rr_ext*cos(Angle_arc_dents_rotor/2+3*pi/2+pos),Rr_ext*sin(Angle_arc_dents_rotor/2+3*pi/2+pos),...
				Rr_int*cos(Angle_arc_dents_rotor/2+3*pi/2+pos),Rr_int*sin(Angle_arc_dents_rotor/2+3*pi/2+pos))
mi_drawline(Rr_ext*cos(-Angle_arc_dents_rotor/2+3*pi/2+pos),Rr_ext*sin(-Angle_arc_dents_rotor/2+3*pi/2+pos),...
				Rr_int*cos(-Angle_arc_dents_rotor/2+3*pi/2+pos),Rr_int*sin(-Angle_arc_dents_rotor/2+3*pi/2+pos))	
mi_drawarc(Rr_int*cos(Angle_arc_dents_rotor/2+pos),Rr_int*sin(Angle_arc_dents_rotor/2+pos),...
				Rr_int*cos(-Angle_arc_dents_rotor/2+pi/2+pos),Rr_int*sin(-Angle_arc_dents_rotor/2+pi/2+pos),...
				Angle_arc_dents_rotor*180/pi,1)
mi_drawarc(Rr_int*cos(Angle_arc_dents_rotor/2+pi/2+pos),Rr_int*sin(Angle_arc_dents_rotor/2+pi/2+pos),...
				Rr_int*cos(-Angle_arc_dents_rotor/2+2*pi/2+pos),Rr_int*sin(-Angle_arc_dents_rotor/2+2*pi/2+pos),...
				Angle_arc_dents_rotor*180/pi,1)
mi_drawarc(Rr_int*cos(Angle_arc_dents_rotor/2+2*pi/2+pos),Rr_int*sin(Angle_arc_dents_rotor/2+2*pi/2+pos),...
				Rr_int*cos(-Angle_arc_dents_rotor/2+3*pi/2+pos),Rr_int*sin(-Angle_arc_dents_rotor/2+3*pi/2+pos),...
				Angle_arc_dents_rotor*180/pi,1)
mi_drawarc(Rr_int*cos(Angle_arc_dents_rotor/2+3*pi/2+pos),Rr_int*sin(Angle_arc_dents_rotor/2+3*pi/2+pos),...
				Rr_int*cos(-Angle_arc_dents_rotor/2+4*pi/2+pos),Rr_int*sin(-Angle_arc_dents_rotor/2+4*pi/2+pos),...
				Angle_arc_dents_rotor*180/pi,1)
mi_addblocklabel((Rr_ext+Rr_int)/2*cos(pos),(Rr_ext+Rr_int)/2*sin(pos))	%Ajoute un point de propriétés aux coordonnées)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel((Rr_ext+Rr_int)/2*cos(pos),(Rr_ext+Rr_int)/2*sin(pos))
mi_setblockprop('Fer_lin',0,0,'<None>',0,0,0)	%Affecte la propriété aux label délectionné
mi_clearselected				
mi_addblocklabel((Rr_ext+Rr_int)/2*cos(pos+pi/4),(Rr_ext+Rr_int)/2*sin(pos+pi/4))	%Ajoute un point de propriétés aux coordonnées)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel((Rr_ext+Rr_int)/2*cos(pos+pi/4),(Rr_ext+Rr_int)/2*sin(pos+pi/4))
mi_addblocklabel((Rr_ext+Rr_int)/2*cos(pos+3*pi/4),(Rr_ext+Rr_int)/2*sin(pos+3*pi/4))	%Ajoute un point de propriétés aux coordonnées)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel((Rr_ext+Rr_int)/2*cos(pos+3*pi/4),(Rr_ext+Rr_int)/2*sin(pos+3*pi/4))
mi_addblocklabel((Rr_ext+Rr_int)/2*cos(pos+5*pi/4),(Rr_ext+Rr_int)/2*sin(pos+5*pi/4))	%Ajoute un point de propriétés aux coordonnées)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel((Rr_ext+Rr_int)/2*cos(pos+5*pi/4),(Rr_ext+Rr_int)/2*sin(pos+5*pi/4))
mi_addblocklabel((Rr_ext+Rr_int)/2*cos(pos+7*pi/4),(Rr_ext+Rr_int)/2*sin(pos+7*pi/4))	%Ajoute un point de propriétés aux coordonnées)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel((Rr_ext+Rr_int)/2*cos(pos+7*pi/4),(Rr_ext+Rr_int)/2*sin(pos+7*pi/4))
mi_setblockprop('Air',0,0,'<None>',0,0,0)	%Affecte la propriété aux label délectionné
mi_clearselected				


%mi setblockprop(’blockname’, automesh, meshsize, ’incircuit’, magdir, group,turns) affecte les matériaux à la zone sélectionnée
%Construction Arbre
mi_drawarc(R_arbre,0,-R_arbre,0,180,1)
mi_drawarc(-R_arbre,0,R_arbre,0,180,1)
mi_addblocklabel(0,0)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel(0,0);  %Sélectionne le label le plus proche des coor
%mi setblockprop(’blockname’, automesh, meshsize, ’incircuit’, magdir, group,turns) affecte les matériaux à la zone sélectionnée
mi_setblockprop('Air',0,0,'<None>',0,0,0)	%Affecte la propriété aux label délectionné
mi_clearselected


%Ajout des conditions aux limites
%mi addboundprop(’propname’, A0, A1, A2, Phi, Mu, Sig, c0, c1, BdryFormat,ia, oa)
mi_addboundprop('Dirichlet', 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0);
mi_selectarcsegment(0,R_stat_ext);
mi_selectarcsegment(0,-R_stat_ext);
mi_setarcsegmentprop(1,'Dirichlet',0,0)
mi_clearselected	%Efface la liste des trucs sélectionnés pour les conditions aux limites



mi_zoomnatural();
mi_saveas('MRV.fem');

mi_analyze();
mi_loadsolution();


##mo_showdensityplot(legend,gscale,upper_B,lower_B,type) Shows the flux density
##plot with options:
##legend Set to 0 to hide the plot legend or 1 to show the plot legend.
##gscale Set to 0 for a colour density plot or 1 for a grey scale density plot.
##upper_B Sets the upper display limit for the density plot.
##lower_B Sets the lower display limit for the density plot.
##type Type of density plot to display. Valid entries are ’mag’, ’real’, and ’imag’
##for magnitude, real component, and imaginary component of B, respectively. Alterna-
##tively, current density can be displayed by specifying ’jmag’, ’jreal’, and ’jimag’
##for magnitude, real component, and imaginary component of J, respectively.

mo_showvectorplot(1,1)

mo_savebitmap(num2str(position(m)))


%param_elec=mo_getcircuitproperties('Induit+') 


closefemm

endfor
