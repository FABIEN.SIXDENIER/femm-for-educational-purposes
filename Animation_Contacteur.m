%Contacteur.m

clear all
close all
clc

pkg load signal
addpath("~/.wine/drive_c/femm42/mfiles");
savepath;

%Création d'un vecteur de position d'entrefer pour l'animation
T=0:pi/50:6*pi;
position=sawtooth(T,0.5)+1;
position=100*position./max(position)+1;

%plot(T,position)

for k=1:length(position)

%Paramètres dimensionnels (Attention donné en mm, comme dans FEMM)
I_bob=5;
Nb_turns=500;

prof=150;	
Larg_pole=150;

dist_bob=3;
entref=position(k);
Haut_pole=Larg_pole;


openfemm;

newdocument(0); % 0 correspond a un problème magnetics

main_maximize;
mi_probdef(0,'millimeters','planar',1e-8,prof,30);

%Construction domaine
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%mi_drawline(x1,y1,x2,y2)
r0=4*Haut_pole;
mi_drawline(0,0,0,r0)
mi_drawline(0,0,r0,0)
%mi_addarc(x1,y1,x2,y2,angle,maxseg)
mi_addarc(r0,0,0,r0,90,1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Construction CM
mi_drawline(0,entref/2,Larg_pole/2,entref/2)
mi_drawline(Larg_pole/2,entref/2,Larg_pole/2,entref/2+Haut_pole)
mi_drawline(Larg_pole/2,entref/2+Haut_pole,1.5*Larg_pole,entref/2+Haut_pole)
mi_drawline(1.5*Larg_pole,entref/2+Haut_pole,1.5*Larg_pole,entref/2)
mi_drawline(1.5*Larg_pole,entref/2,2*Larg_pole,entref/2)
mi_drawline(2*Larg_pole,entref/2,2*Larg_pole,Haut_pole+Larg_pole/2+entref/2)
mi_drawline(2*Larg_pole,Haut_pole+Larg_pole/2+entref/2,0,Haut_pole+Larg_pole/2+entref/2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Construction bob
mi_drawline(Larg_pole/2+dist_bob,0,Larg_pole/2+dist_bob,Haut_pole-dist_bob)
mi_drawline(Larg_pole/2+dist_bob,Haut_pole-dist_bob,1.5*Larg_pole-dist_bob,Haut_pole-dist_bob)
mi_drawline(1.5*Larg_pole-dist_bob,Haut_pole-dist_bob,1.5*Larg_pole-dist_bob,0)


%propriétés des matériaux
%exemple mi_addmaterial(’matname’, mu x, mu y, H c, J, Cduct, Lam d, Phi hmax, lam fill,LamType, Phi hx, Phi hy, nstr, dwire)
%import de la bibliothèque de matériaux
mu_0=4e-7*pi;
mu_r=5000;
mi_getmaterial("Air")	%Air
mi_getmaterial("M-27 Steel")
mi_addmaterial('Cuivre', 1, 1, 0, 0 ,0, 0, 0, 1, 0, 0, 0);   % 
mi_addmaterial('Fer_lin', mu_r, mu_r, 0, 0, 0, 0, 0, 1, 0, 0, 0); % 


%Ajout des conditions aux limites
%mi addboundprop(’propname’, A0, A1, A2, Phi, Mu, Sig, c0, c1, BdryFormat,ia, oa)
mi_addboundprop('Dirichlet', 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0);
mi_addboundprop('Mixte', 0, 0, 0, 0, 0, 0, 1/(mu_0*r0*1e-3), 0, 2,0, 0);

%Conditions de Dirichlet sur l'axe de symétrie
mi_selectsegment(0,r0);
mi_selectsegment(0,entref/2);
mi_selectsegment(0,Haut_pole);
mi_setsegmentprop("Dirichlet",0,1,0,0,"<None>");
%Conditions mixte sur la frontière externe du domaine
mi_clearselected	%Efface la liste des trucs sélectionnés pour les conditions aux limites
mi_selectarcsegment(r0,r0);
mi_setarcsegmentprop(1,'Mixte',0,0)

%Ajout des propriétés 'Air' pour l'air
mi_addblocklabel(r0/sqrt(3),r0/sqrt(3));
mi_selectlabel(r0/sqrt(3),r0/sqrt(3));
mi_setblockprop('Air',0,0,'<None>',0,0,0)	%Affecte la propriété aux label délectionné	
mi_clearselected


%Ajout des propriétés 'Fer_lin' pour l'air
mi_addblocklabel(Larg_pole/4, Larg_pole/4 +entref/2);
mi_selectlabel( Larg_pole/4, Larg_pole/4+entref/2);
mi_setblockprop('M-27 Steel',0,0,'<None>',0,0,0)	%Affecte la propriété aux label délectionné	
mi_clearselected


%Ajout des propriétés 'Cuivre' pour la bobine et définition du circuit
%mi_addcircprop('circuit name', Val_i , circuit_type)
mi_addcircprop('Bob', I_bob , 1)
mi_addblocklabel(Larg_pole+dist_bob,Larg_pole/2);
mi_selectlabel( Larg_pole+dist_bob,Larg_pole/2);
%mi setblockprop(’blockname’, automesh, meshsize, ’incircuit’, magdir, group,turns) affecte les matériaux à la zone sélectionnée
mi_setblockprop('Cuivre',0,0,'Bob',0,1,Nb_turns)	%Affecte la propriété aux label délectionné	
mi_clearselected

mi_saveas('Contacteur.fem');


mi_analyze();
mi_loadsolution();


mo_showvectorplot(1,1)

mo_savebitmap(num2str(k))


mi_zoomnatural();



closefemm

end
