clear all
close all
clc

addpath("~/.wine/drive_c/femm42/mfiles");
savepath;

%Paramètres dimensionnels (Attention donné en mm, comme dans FEMM)

%profondeur (mm)
prof=150;	


%Stator
Ds_int=130;
Ds_ext=150;

%entrefer
entrefer=1;

%Rotor
Dr_int=70;
Dr_ext=80;

%Arbre moteur
D_arbre=20;


%Carac Inducteur
N_inducteur=200;
I_ex=2;		%Mettre 0 si on ne veut pas alimenter l'inducteur

%Courant Induit
N_induit=10
I=-5; 		%Mettre 0 si on ne veut pas alimenter l'induit

 
%Construction encoches
ang_encoches=pi/12;
angles=[-pi/2:ang_encoches:3*pi/2];
 position=linspace(0,2*ang_encoches,24);
 
 for m=1:1:length(position)
pos=position(m);


%closefemm
openfemm;

newdocument(0); % 0 correspond a un problème magnétostatique

main_maximize;
mi_probdef(0,'millimeters','planar',1e-8,prof,30);

%propriétés des matériaux
%exemple mi_addmaterial(’matname’, mu x, mu y, H c, J, Cduct, Lam d, Phi hmax, lam fill,LamType, Phi hx, Phi hy, nstr, dwire)
mu_r=5000;
mi_addmaterial('Air', 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0);       % 
mi_addmaterial('Cuivre', 1, 1, 0, 0 ,0, 0, 0, 1, 0, 0, 0);   % 
mi_addmaterial('Fer_lin', mu_r, mu_r, 0, 0, 0, 0, 0, 1, 0, 0, 0); % 

%%mi_addcircprop('circuit name', Val_i , circuit_type)
%Ajoute un circuit avec son nom, la valeur du courant et le type de connection (0 : parallel ; 1 : serie)
mi_addcircprop('Inducteur+', I_ex , 1)
mi_addcircprop('Inducteur-', -I_ex , 1)
mi_addcircprop('Induit+', I , 1)
mi_addcircprop('Induit-', -I , 1)


%Construction stator
mi_drawarc(Ds_ext/2,0,-Ds_ext/2,0,180,1)
mi_drawarc(-Ds_ext/2,0,Ds_ext/2,0,180,1)
mi_drawarc(Ds_int/2,0,-Ds_int/2,0,180,1)
mi_drawarc(-Ds_int/2,0,Ds_int/2,0,180,1)
mi_addblocklabel(0,Ds_int/4+Ds_ext/4)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel(0,Ds_int/4+Ds_ext/4);  %Sélectionne le label le plus proche
%mi setblockprop(’blockname’, automesh, meshsize, ’incircuit’, magdir, group,turns) affecte les matériaux à la zone sélectionnée
mi_setblockprop('Fer_lin',0,0,'<None>',0,0,0)	%Affecte la propriété aux label délectionné
mi_clearselected

%Construction rotor
mi_drawarc(Dr_ext/2,0,-Dr_ext/2,0,180,1)
mi_drawarc(-Dr_ext/2,0,Dr_ext/2,0,180,1)
mi_drawarc(Dr_int/2,0,-Dr_int/2,0,180,1)
mi_drawarc(-Dr_int/2,0,Dr_int/2,0,180,1)
mi_addblocklabel(0,Dr_int/4+D_arbre/4)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel(Dr_int/4+D_arbre/4,0);  %Sélectionne le label le plus proche des coor
%mi setblockprop(’blockname’, automesh, meshsize, ’incircuit’, magdir, group,turns) affecte les matériaux à la zone sélectionnée
mi_setblockprop('Fer_lin',0,0,'<None>',0,1,0)	%Affecte la propriété aux label délectionné
mi_clearselected	%Efface la sélection
mi_addblocklabel(0,Dr_ext/4+Ds_int/4)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel(0,Dr_ext/4+Ds_int/4);  %Sélectionne le label le plus proche des coor
%mi setblockprop(’blockname’, automesh, meshsize, ’incircuit’, magdir, group,turns) affecte les matériaux à la zone sélectionnée
mi_setblockprop('Air',0,0,'<None>',0,1,0)	%Affecte la propriété aux label délectionné
mi_clearselected



%Construction Arbre
mi_drawarc(D_arbre/2,0,-D_arbre/2,0,180,1)
mi_drawarc(-D_arbre/2,0,D_arbre/2,0,180,1)
mi_addblocklabel(0,0)	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel(0,0);  %Sélectionne le label le plus proche des coor
%mi setblockprop(’blockname’, automesh, meshsize, ’incircuit’, magdir, group,turns) affecte les matériaux à la zone sélectionnée
mi_setblockprop('Air',0,0,'<None>',0,1,0)	%Affecte la propriété aux label délectionné
mi_clearselected



%Construction poles
angle_pole=60;
%arcs proches de l'entrefer
mi_drawarc((Dr_ext/2+entrefer)*cos(-angle_pole*pi/180),(Dr_ext/2+entrefer)*sin(-angle_pole*pi/180),(Dr_ext/2+entrefer)*cos(angle_pole*pi/180),(Dr_ext/2+entrefer)*sin(angle_pole*pi/180),2*angle_pole,1)
mi_drawarc((Dr_ext/2+entrefer)*cos(-angle_pole*pi/180 + pi),(Dr_ext/2+entrefer)*sin(- angle_pole*pi/180 + pi),(Dr_ext/2+entrefer)*cos(angle_pole*pi/180 + pi),(Dr_ext/2+entrefer)*sin(angle_pole*pi/180 + pi),2*angle_pole,1)
mi_addblocklabel( (Dr_ext/2+entrefer + Ds_int/2)/2,0 )	%Ajoute un point de propriétés aux coordonnées
mi_addblocklabel( -(Dr_ext/2+entrefer + Ds_int/2)/2,0 )	%Ajoute un point de propriétés aux coordonnées
mi_selectlabel( (Dr_ext/2+entrefer + Ds_int/2)/2,0 );
mi_selectlabel( -(Dr_ext/2+entrefer + Ds_int/2)/2,0 );
mi_setblockprop('Fer_lin',0,0,'<None>',0,0,0)	%Affecte la propriété aux label délectionné
%mi setblockprop(’blockname’, automesh, meshsize, ’incircuit’, magdir, group,turns) affecte les matériaux à la zone sélectionnée
mi_clearselected

%Aretes du pole
	%Calcul de la'angle pour intersecter la culasse stator externe
beta=180/pi*asin((Dr_ext/2+entrefer)*sin(angle_pole*pi/180)/(Ds_int/2));
	%pole gauche
mi_drawline((Dr_ext/2+entrefer)*cos(-angle_pole*pi/180 + pi),(Dr_ext/2+entrefer)*sin(- angle_pole*pi/180 + pi), -Ds_int/2*cos(beta*pi/180) , (Dr_ext/2+entrefer)*sin(- angle_pole*pi/180 + pi))
mi_drawline((Dr_ext/2+entrefer)*cos(-angle_pole*pi/180 + pi),-(Dr_ext/2+entrefer)*sin(- angle_pole*pi/180 + pi), -Ds_int/2*cos(beta*pi/180) , -(Dr_ext/2+entrefer)*sin(- angle_pole*pi/180 + pi))
	%pole droit
mi_drawline(-(Dr_ext/2+entrefer)*cos(-angle_pole*pi/180 + pi),(Dr_ext/2+entrefer)*sin(- angle_pole*pi/180 + pi), Ds_int/2*cos(beta*pi/180) , (Dr_ext/2+entrefer)*sin(- angle_pole*pi/180 + pi))
mi_drawline(-(Dr_ext/2+entrefer)*cos(-angle_pole*pi/180 + pi),-(Dr_ext/2+entrefer)*sin(- angle_pole*pi/180 + pi), Ds_int/2*cos(beta*pi/180) , -(Dr_ext/2+entrefer)*sin(- angle_pole*pi/180 + pi))
	%arc_intermédiaire externe

	X_corne=(Dr_ext/2+entrefer+Ds_int/2)/2;
	Y_corne=Ds_ext/2-Ds_int/2;
	Dist=norm([X_corne, Y_corne]	);
	gamma=180/pi*asin(Y_corne/Dist);	%angle bord interne jambe
	%mi_drawline( X_corne,-Y_corne,X_corne,Y_corne		);
	%mi_drawline( -X_corne,-Y_corne,-X_corne,Y_corne		);
	mi_drawline(X_corne,Y_corne,  (-(Dr_ext/2+entrefer)*cos(-angle_pole*pi/180 + pi)+Ds_int/2*cos(beta*pi/180))/2 , (Dr_ext/2+entrefer)*sin(- angle_pole*pi/180 + pi) )	
	mi_drawline(X_corne,-Y_corne,  (-(Dr_ext/2+entrefer)*cos(-angle_pole*pi/180 + pi)+Ds_int/2*cos(beta*pi/180))/2 , -(Dr_ext/2+entrefer)*sin(- angle_pole*pi/180 + pi) )	
	mi_drawline(-X_corne,Y_corne,  -(-(Dr_ext/2+entrefer)*cos(-angle_pole*pi/180 + pi)+Ds_int/2*cos(beta*pi/180))/2 , (Dr_ext/2+entrefer)*sin(- angle_pole*pi/180 + pi) )	
	mi_drawline(-X_corne,-Y_corne,  -(-(Dr_ext/2+entrefer)*cos(-angle_pole*pi/180 + pi)+Ds_int/2*cos(beta*pi/180))/2 , -(Dr_ext/2+entrefer)*sin(- angle_pole*pi/180 + pi) )	
	psi=(180/pi)*(2*Y_corne/Ds_int);
	mi_drawline(X_corne,Y_corne, Ds_int/2*cos(psi*pi/180), Ds_int/2*sin(psi*pi/180) )	
	mi_drawline(X_corne,-Y_corne, Ds_int/2*cos(psi*pi/180), -Ds_int/2*sin(psi*pi/180) )	
	mi_drawline(-X_corne,-Y_corne, -Ds_int/2*cos(psi*pi/180), -Ds_int/2*sin(psi*pi/180) )	
	mi_drawline(-X_corne,Y_corne, -Ds_int/2*cos(psi*pi/180), Ds_int/2*sin(psi*pi/180) )	
	%Affectation des inducteurs
	coeff=0.5;
	mi_addblocklabel( (Dr_ext/2+entrefer + Ds_int/2)/2, coeff*(Dr_ext/2+entrefer + Ds_int/2)/2);
	mi_addblocklabel( -(Dr_ext/2+entrefer + Ds_int/2)/2, coeff*(Dr_ext/2+entrefer + Ds_int/2)/2);
	mi_selectlabel( -(Dr_ext/2+entrefer + Ds_int/2)/2,coeff*(Dr_ext/2+entrefer + Ds_int/2)/2 );
	mi_selectlabel( (Dr_ext/2+entrefer + Ds_int/2)/2,coeff*(Dr_ext/2+entrefer + Ds_int/2)/2 );
	mi_setblockprop('Cuivre',0,0,'Inducteur+',0,0,N_inducteur)	%Affecte la propriété aux label délectionné
	%mi setblockprop(’blockname’, automesh, meshsize, ’incircuit’, magdir, group,turns) affecte les matériaux à la zone sélectionnée
	mi_clearselected
	coeff=0.5;
	mi_addblocklabel( (Dr_ext/2+entrefer + Ds_int/2)/2, -coeff*(Dr_ext/2+entrefer + Ds_int/2)/2);
	mi_addblocklabel( -(Dr_ext/2+entrefer + Ds_int/2)/2, -coeff*(Dr_ext/2+entrefer + Ds_int/2)/2);
	mi_selectlabel( -(Dr_ext/2+entrefer + Ds_int/2)/2, - coeff*(Dr_ext/2+entrefer + Ds_int/2)/2 );
	mi_selectlabel( (Dr_ext/2+entrefer + Ds_int/2)/2, - coeff*(Dr_ext/2+entrefer + Ds_int/2)/2 );
	mi_setblockprop('Cuivre',0,0,'Inducteur-',0,0,N_inducteur)	%Affecte la propriété aux label délectionné
	%mi setblockprop(’blockname’, automesh, meshsize, ’incircuit’, magdir, group,turns) affecte les matériaux à la zone sélectionnée
	mi_clearselected
	
	

%position en rad pour simulation et gif animé
 
 

	  for k=1:length(angles)
		  mi_drawline(Dr_int/2*cos(angles(k)+pos),Dr_int/2*sin(angles(k) +pos ),Dr_ext/2*cos(angles(k) +pos ),Dr_ext/2*sin(angles(k) + pos))
		  %Ajout des propriétés 'Fer' pour les dents ou 'Cuivre' pour les encoches
		  mi_addblocklabel( (Dr_int/4+Dr_ext/4) *cos(angles(k)+ pos + ang_encoches/2 ), (Dr_int/4+Dr_ext/4)*sin(angles(k) + pos + ang_encoches/2));
		  mi_selectlabel( (Dr_int/4+Dr_ext/4) *cos(angles(k)+ pos + ang_encoches/2 ), (Dr_int/4+Dr_ext/4)*sin(angles(k) +pos + ang_encoches/2));
		  mi_setblockprop('Fer_lin',0,0,'<None>',0,1,0)	%Affecte la propriété aux label délectionné	
		  if rem(k,2)~=0
			  if (angles(k)+ pos + ang_encoches/2>-pi/2) & (angles(k) + pos +ang_encoches/2<pi/2)
				  mi_setblockprop('Cuivre',0,0,'Induit+',0,1,N_induit)	%Affecte la propriété aux label délectionné
			  elseif (angles(k) + pos +ang_encoches/2<3*pi/2) & (angles(k)+pos + ang_encoches/2>pi/2)	
				  mi_setblockprop('Cuivre',0,0,'Induit-',0,1,N_induit)	%Affecte la propriété aux label délectionné
			  endif 
		  endif
		 mi_clearselected
	  endfor




%Ajout des conditions aux limites
%mi addboundprop(’propname’, A0, A1, A2, Phi, Mu, Sig, c0, c1, BdryFormat,ia, oa)
mi_addboundprop('Dirichlet', 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0);
mi_selectarcsegment(0,Ds_ext);
mi_selectarcsegment(0,-Ds_ext);
mi_setarcsegmentprop(1,'Dirichlet',0,0)
mi_clearselected	%Efface la liste des trucs sélectionnés pour les conditions aux limites


mi_zoomnatural();
mi_saveas('Test_MCC.fem');

mi_analyze();
mi_loadsolution();


##mo_showdensityplot(legend,gscale,upper_B,lower_B,type) Shows the flux density
##plot with options:
##legend Set to 0 to hide the plot legend or 1 to show the plot legend.
##gscale Set to 0 for a colour density plot or 1 for a grey scale density plot.
##upper_B Sets the upper display limit for the density plot.
##lower_B Sets the lower display limit for the density plot.
##type Type of density plot to display. Valid entries are ’mag’, ’real’, and ’imag’
##for magnitude, real component, and imaginary component of B, respectively. Alterna-
##tively, current density can be displayed by specifying ’jmag’, ’jreal’, and ’jimag’
##for magnitude, real component, and imaginary component of J, respectively.

mo_showvectorplot(1,1)

mo_savebitmap(num2str(m))


%param_elec=mo_getcircuitproperties('Induit+') 


closefemm


end
